const records = [
  {
    id: 'testclient',
    name: 'Test Client',
    redirectUri: 'https://www.getpostman.com/oauth2/callback',
    secret: 'supersecret',
  },
];

module.exports.findById = function (id, cb) {
  for (let i = 0, len = records.length; i < len; i++) {
    const record = records[i];
    if (record.id === id) {
      return cb(null, record);
    }
  }
  return cb(null, null);
};
