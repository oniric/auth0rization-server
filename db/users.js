const records = [
  {
    id: 1,
    googleId: null,
    password: '$2b$10$X1tKobs0muEOPxMQAh084.xnMghC53COAhRO5SQi.mIlBmVHzNXa2',
    displayName: 'Jack',
    email: 'jack@example.com',
    codes: [],
    tokens: [],
  },
  {
    id: 2,
    googleId: null,
    password: '$2b$10$YK5isaG/0EVZS4RULnVmpOCEqOftC5gx9Ep2o49BDx8TOiU3bYfI6',
    displayName: 'Jill',
    email: 'jill@example.com',
    codes: [],
    tokens: [],
  },
];

module.exports.findByEmail = function (email, cb) {
  for (let i = 0, len = records.length; i < len; i++) {
    const record = records[i];
    if (record.email === email) {
      return cb(null, record);
    }
  }
  return cb(null, null);
};

module.exports.findByEmailWithPassword = function (email, cb) {
  for (let i = 0, len = records.length; i < len; i++) {
    const record = records[i];
    if (record.password !== null && record.email === email) {
      return cb(null, record);
    }
  }
  return cb(null, null);
};

module.exports.findById = function (id, cb) {
  for (let i = 0, len = records.length; i < len; i++) {
    const record = records[i];
    if (record.id === id) {
      return cb(null, record);
    }
  }
  return cb(null, null);
};

module.exports.findByGoogleId = function (googleId, cb) {
  for (let i = 0, len = records.length; i < len; i++) {
    const record = records[i];
    if (record.googleId === googleId) {
      return cb(null, record);
    }
  }
  return cb(null, null);
};

module.exports.findByGitHubId = function (githubId, cb) {
  for (let i = 0, len = records.length; i < len; i++) {
    const record = records[i];
    if (record.githubId === githubId) {
      return cb(null, record);
    }
  }
  return cb(null, null);
};

module.exports.findByToken = function (token, cb) {
  for (let i = 0, len = records.length; i < len; i++) {
    const record = records[i];
    for (let j = 0, len2 = record.tokens.length; j < len2; j++) {
      if (record.tokens[j].token === token) {
        return cb(null, record, record.tokens[j].scope);
      }
    }
  }
  return cb(null, null);
};

module.exports.createUserFromGoogleProfile = function (profile, cb) {
  const id = records.length + 1;

  // get first email in the profile
  const email = profile.emails[0].value;
  const name = profile.name || {};

  const familyName = name.familyName || null;
  const givenName = name.givenName || null;

  const user = {
    id: id,
    googleId: profile.id,
    email: email,
    password: null,
    displayName: profile.displayName,
    familyName: familyName,
    givenName: givenName,
    gender: profile.gender || null,
    codes: [],
    tokens: [],
  };

  records.push(user);

  return cb(null, user);
};

module.exports.createUserFromGitHubProfile = function (profile, cb) {
  const id = records.length + 1;

  // get first email in the profile
  const email = profile.emails[0].value;

  const user = {
    id: id,
    githubId: profile.id,
    email: email,
    password: null,
    displayName: profile.displayName,
    givenName: null,
    familyName: null,
    codes: [],
    tokens: [],
  };

  records.push(user);

  return cb(null, user);
};

module.exports.findAuthCodeByCode = function (code, cb) {
  for (let i = 0, len = records.length; i < len; i++) {
    const record = records[i];
    for (let j = 0, len2 = record.codes.length; j < len2; j++) {
      if (record.codes[j].code === code) {
        return cb(null, record.codes[j]);
      }
    }
  }
  return cb(null, null);
};

module.exports.createFromFormParameters = function (email, displayName, hashedPassword, cb) {
  const id = records.length + 1;

  const user = {
    id: id,
    googleId: null,
    email: email,
    password: hashedPassword,
    displayName: displayName,
    codes: [],
    tokens: [],
  };

  records.push(user);

  return cb(null, user);
};
