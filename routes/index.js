const express = require('express');
const passport = require('passport');
const csrf = require('csurf');
const bcrypt = require('bcrypt');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;
const oauth2Controller = require('./oauth2');
const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

const csrfProtection = csrf();

const User = require('../db/users');

function checkEmailNotInUse(value) {
  return new Promise(function (resolve, reject) {
    User.findByEmailWithPassword(value, function (err, user) {
      if (err) {
        return reject(err);
      }

      if (user) {
        return reject('Email already in use');
      }

      return resolve();
    });
  });
}

function checkPasswordConfirmed(value, { req }) {
  if (value !== req.body.password_confirm) {
    throw new Error('Password confirmation does not match password');
  }

  return true;
}

const router = express.Router();

router.get('/', function (req, res) {
  res.render('index', { title: 'Auth0rization Server', user: req.user });
});

router.get('/register', csrfProtection, function (req, res) {
  res.render('register', { csrfToken: req.csrfToken() });
});

router.post('/register', csrfProtection,
  body('email')
    .not().isEmpty().withMessage('Email is required')
    .isString()
    .isEmail()
    .custom(checkEmailNotInUse)
    .trim(),
  body('display_name')
    .not().isEmpty().withMessage('Display name is required')
    .isString()
    .isLength({ min: 3, max: 20 }).withMessage('Display name must be between 3 and 20 characters long')
    .matches(/^[-a-zA-Z0-9_]+$/, 'i').withMessage('Display name must contains only letters, digits and underscores')
    .trim(),
  body('password')
    .not().isEmpty().withMessage('Password is required')
    .isString()
    .isLength({ min: 8 }).withMessage('Password should be at least 8 characters long')
    .custom(checkPasswordConfirmed).withMessage('Password confirmation does not match password'),
  sanitizeBody('email').trim(),
  sanitizeBody('display_name').trim(),
  function (req, res) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.render('register', { errors: errors.array(), csrfToken: req.csrfToken() });
      return;
    }

    // data is good, so we register the user
    const email = req.body.email;
    const hashedPassword = bcrypt.hashSync(req.body.password, 10);
    const displayName = req.body.display_name;
    User.createFromFormParameters(email, displayName, hashedPassword, function () {
      res.redirect('/login');
    });
});

router.get('/me',
  ensureLoggedIn('/login'),
  function (req, res) {
    res.render('profile', { user: req.user });
});

router.get('/login', csrfProtection,
  function (req, res) {
    res.render('login', { csrfToken: req.csrfToken() });
});

router.post('/login', csrfProtection,
  passport.authenticate('local', { successReturnToOrRedirect: '/', failureRedirect: '/login' }),
  function (req, res) {
    res.redirect('/');
});

router.get('/logout',
  function (req, res) {
    req.logout();
    res.redirect('/login');
});

router.get('/auth/google',
  passport.authenticate('google', { scope: ['openid email profile'] })
);

router.get('/auth/google/callback', 
  passport.authenticate('google', { successReturnToOrRedirect: '/', failureRedirect: '/login' }),
  function (req, res) {
    res.redirect('/');
});

router.get('/auth/github',
  passport.authenticate('github')
);

router.get('/auth/github/callback', 
  passport.authenticate('github', { successReturnToOrRedirect: '/', failureRedirect: '/login' }),
  function (req, res) {
    res.redirect('/');
});

// create endpoint handlers for oauth2 authorize
router.get('/oauth/authorize',
  ensureLoggedIn('/login'),
  oauth2Controller.authorization,
);

// create endpoint handlers for oauth2 token
router.post('/oauth/token',
  passport.authenticate('oauth2-client-password', { session: false }),
  oauth2Controller.token,
);

router.post('/decision',
  ensureLoggedIn('/login'),
  oauth2Controller.decision
);

// OpenID Connect basic support
router.get('/userinfo', 
  passport.authenticate('bearer', { session: false }),
  function (req, res) {
    const claims = req.authInfo.scope;
    const user = req.user;
    const result = {};

    // send error if openid scope is not contained in the token
    if (claims.indexOf('openid') === -1) {
      res.status(401);
      res.json({
        error: 'invalid_token',
        error_description: 'Token does not have openid scope'
      });
      return;
    }

    result.sub = user.id;

    if (claims.indexOf('email') !== -1) {
      result.email = user.email;
    }

    // profile URL is just a fake for now
    if (claims.indexOf('profile') !== -1) {
      result.profile = 'http://localhost:3000/user/' + user.id;

      // additional data if the user logged in through Google
      if (user.familyName !== null) {
        result.family_name = user.familyName;
      }

      if (user.givenName !== null) {
        result.given_name = user.givenName;
      }

      if (user.givenName === null && user.familyName === null) {
        result.name = user.displayName;
      }

      if (user.gender !== null) {
        result.gender = user.gender;
      }
    }

    res.json(result);
});

module.exports = router;
