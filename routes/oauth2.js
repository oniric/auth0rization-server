const oauth2orize = require('oauth2orize');
const crypto = require('crypto');

const Client = require('../db/clients');
const User = require('../db/users');

const server = oauth2orize.createServer();

function generateToken() {
  return crypto.randomBytes(48).toString('hex');
}

function isValidClaim(claim) {
  const claims = ['openid', 'profile', 'email'];

  for (let i = 0, len = claims.length; i < len; i++) {
    if (claim === claims[i]) {
      return true;
    }
  }

  return false;
}

function sanitizeScope(scope) {
  const sanitizedScope = [];

  scope = scope || [];

  // check scope is a valid and supported one
  for (let i = 0, len = scope.length; i < len; i++) {
    if (isValidClaim(scope[i]) && sanitizedScope.indexOf(scope[i]) === -1) {
      sanitizedScope.push(scope[i]);
    }
  }

  return sanitizedScope;
}

function checkTokenIsValidForScope(tokenScopes, scopes) {
  var intersect = scopes.filter(function (s) {
    return tokenScopes.indexOf(s) !== -1;
  });

  return intersect.length === scopes.length;
}

server.serializeClient(function (client, done) {
  return done(null, client.id);
})

server.deserializeClient(function (id, done) {
  Client.findById(id, function (err, client) {
    if (err) {
      return done(err);
    }
    return done(null, client);
  });
});

server.grant(oauth2orize.grant.code(function (client, redirectUri, user, ares, done) {
  const scopeArray = sanitizeScope(ares.scope);

  // we require at least one valid claim
  if (!scopeArray.length) {
    return done(null, false);
  }

  const code = generateToken();

  const ac = {
    code: code,
    clientId: client.id,
    redirectUri: redirectUri,
    userId: user.id,
    scope: scopeArray,
    used: false,
    expirationTime: Date.now() + 5 * 60 * 1000, // code expires in 5 minutes (in ms)
  };

  user.codes.push(ac);

  return done(null, code);
}));

server.exchange(oauth2orize.exchange.code(function (client, code, redirectUri, done) {
  User.findAuthCodeByCode(code, function (err, authCode) {
    if (err) {
      return done(err);
    }

    if (!authCode) {
      return done(null, false);
    }

    // validate the code was not used before
    if (authCode.used) {
      return done(null, false);
    }

    // check correct client is trying to claim the code
    if (client.id !== authCode.clientId) {
      return done(null, false);
    }

    // validate the redirectUri
    if (redirectUri !== authCode.redirectUri) {
      return done(null, false);
    }

    // validate expiration time
    if (Date.now() > authCode.expirationTime) {
      return done(null, false);
    }

    // mark the code as used
    authCode.used = true;
    
    const token = generateToken();

    const at = {
      clientId: client.id,
      token: token,
      expirationTime: Date.now() + 60 * 60 * 1000, // code expires in 60 minutes (in ms)
      scope: authCode.scope,
    };

    User.findById(authCode.userId, function (err, user) {
      if (err) {
        return done(null, false);
      }

      if (!user) {
        return done(null, false);
      }

      user.tokens.push(at);

      return done(null, token);
    });
  });
}));

// User authorization endpoint
module.exports.authorization = [
  server.authorization(function (clientId, redirectUri, scope, done) {
    const scopeArray = sanitizeScope(scope);

    if (!scopeArray.length) {
      return done(null, false);
    }

    Client.findById(clientId, function (err, client) {
      if (err) {
        return done(err);
      }
      // check redirect uri against client configuration
      if (client.redirectUri !== redirectUri) {
        return done(null, false);
      }
      return done(null, client, redirectUri);
    });
  }, function (client, user, scope, done) {
    const scopeArray = sanitizeScope(scope);

    if (!scopeArray.length) {
      return done(null, false);
    }

    // Check if the request qualifies for immediate approval
    for (let i = 0, len = user.tokens.length; i < len; i++) {
      const token = user.tokens[i];
      if (token.clientId === client.id) {
        // check all the required scopes are part of the found token
        if (checkTokenIsValidForScope(token.scope, scopeArray)) {
          return done(null, true, { scope: scope });
        }
      }
    }

    return done(null, false);
  }),
  function (req, res) {
    const reqScope = req.oauth2.req.scope;
    const client = req.oauth2.client;
    const scope = sanitizeScope(reqScope);

    res.render('consent', { transactionID: req.oauth2.transactionID, user: req.user, client: client, scope: scope });
  },
];

// User decision endpoint
module.exports.decision = [
  // pass the scope from the consent dialog to the grant callback
  server.decision(function (req, done) {
    done(null, { scope: req.oauth2.req.scope });
  }),
];

// Application client token exchange endpoint
module.exports.token = [
  server.token(),
  server.errorHandler(),
];
