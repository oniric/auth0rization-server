const passport = require('passport');
const bcrypt = require('bcrypt');
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const BearerStrategy = require('passport-http-bearer').Strategy;
const ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;
const GitHubStrategy = require('passport-github').Strategy;

const User = require('./db/users');
const Client = require('./db/clients');

passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(function (id, done) {
  User.findById(id, function (err, user) {
    done(err, user);
  });
});

// authentication setup
passport.use(new LocalStrategy(
  function (email, password, done) {
    User.findByEmailWithPassword(email, function (err, user) {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false);
      }
      if (!bcrypt.compareSync(password, user.password)) {
        return done(null, false);
      }
      return done(null, user);
    });
  }
));

passport.use(new GoogleStrategy({
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: 'http://localhost:3000/auth/google/callback',
  state: true,
}, function (accessToken, refreshToken, profile, done) {
  User.findByGoogleId(profile.id, function (err, user) {
    if (err) {
      return done(err);
    }

    if (user) {
      return done(null, user);
    }

    User.createUserFromGoogleProfile(profile, function (err, user) {
      return done(null, user);
    });
  });
}));

passport.use(new GitHubStrategy({
  clientID: process.env.GITHUB_CLIENT_ID,
  clientSecret: process.env.GITHUB_CLIENT_SECRET,
  callbackURL: 'http://localhost:3000/auth/github/callback',
  state: true,
}, function (accessToken, refreshToken, profile, done) {
  User.findByGitHubId(profile.id, function (err, user) {
    if (err) {
      return done(err);
    }

    if (user) {
      return done(null, user);
    }

    User.createUserFromGitHubProfile(profile, function (err, user) {
      return done(null, user);
    });
  });
}));

passport.use(new ClientPasswordStrategy(
  function (clientId, clientSecret, done) {
    Client.findById(clientId, function (err, client) {
      if (err) {
        return done(err);
      }
      if (!client) {
        return done(null, false);
      }
      if (client.secret !== clientSecret) {
        return done(null, false);
      }
      return done(null, client);
    });
  }
));

passport.use(new BearerStrategy(
  function (token, done) {
    User.findByToken(token, function (err, user, scope) {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false);
      }

      // check that the token is not expired
      for (let i = 0, len = user.tokens.length; i < len; i++) {
        if (user.tokens[i].token === token) {
          if (Date.now() > user.tokens[i].expirationTime) {
            return done(null, false);
          }
        }
      }

      // if user is found the scope parameter is also set
      return done(null, user, { scope: scope });
    });
  }
));
